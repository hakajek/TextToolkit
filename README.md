# TextToolkit

This J2ME application (or "MIDlet") provides a few ways to manipulate text, including some very basic encoding and encryption tools. Inspired by a much more comprehensive project called CyberChef, these tools are used in a layer based manner, so that several techniques can be applied in succession. TextToolkit is written to be fairly modular, so adding more tools to it should be trivial.

---

* [Usage](#usage)  
* [Compatibility](#compatibility)  
* [Writing and adding new modules](#writing-and-adding-new-modules)  

## Usage

On opening the application the user is first prompted to input some text, after which the main screen is shown. This is where modules can be added to alter the text, either through a button or a menu, depending on the device's MIDP implementation. The following modules are available:

 - Base64 – decode or encode base64 data
 - Conversion – convert to/from text, hexadecimal, decimal, and binary
 - Morse code – decode or encode Morse code
 - Replace – replace one or more characters with other characters
 - Substitution – rotate the character set N places (Caesar cipher, ROT13)
 - Textflow – reverse the text
 - Viginère – encrypt or decrypt a Viginère cipher
 - XOR – key-based XOR encryption

![adding-modules](assets/figure_1_v02.png)

The modules are added in a stack, where each modifies the output of the one above and passes the result to the one below. Press up/down to select modules, and left/right to switch between different modes of operation. More options can usually be found in the "settings" menu item. From the menu you can also delete the module, toggle it on and off, or move it up and down the stack.

![settings-menu](assets/figure_2_v02.png)

The initial text can always be changed by selecting the topmost (input) module and pressing "settings" in the menu.

At the bottom of the screen is the result of the various operations. If the output is too long to be displayed in its entirety, press down until "Result" is selected, then press right to scroll line by line.

## Compatibility

TextToolkit requires CLDC 1.1, and MIDP 1.0 or higher. No additional APIs needed.

## Writing and adding new modules

Each module extends the `Module` class, which implements a set of methods that are overridden as needed. In particular:

 - `paintContent` – define the visual appearance of the module
 - `interact` – define behavior on interaction (left/right button presses)
 - `format` – take a `String` input, modify it in some way, and return it
 - `getSettings` – build a settings screen for the module
 - `commandAction` – if the module has a settings screen, define the behavior of the `saveCMD` and `cancelCMD` buttons here

When a new module has been written, it has to be registered in the `TTCanvas` class. First add its name to the `moduleTypes` array, then locate the `makeMod` method and add the new module to the `switch` statement. Its number here must correspond to its index in the `moduleTypes` array.

