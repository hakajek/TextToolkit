/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.microedition.lcdui.*;

public class Xor extends Module {
    private TextField tfKey;
    private String key;
    private String mode;

    public Xor(TTCanvas c, Display d)
    {
        super(c, d);
        this.key = "key";
        this.mode = "on";
    }

    // Handle input and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled && mode == "on") {
            byte[] bytes = input.getBytes();
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] ^= key.charAt(i % key.length());
            }
            output = new String(bytes);
        }
        return output;
    }

    // Toggle mode
    protected void interact(int dir)
    {
        if (mode == "on") {
            mode = "off";
        } else {
            mode = "on";
        }
    }

    // Draw the contents of the Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        String str = "XOR [" + mode + "]";
        g.drawString(str, PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("XOR settings");
        tfKey = new TextField("Key:", key, 50, TextField.ANY);
        settings.append(tfKey);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            key = tfKey.getString();
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
