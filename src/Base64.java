/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.microedition.lcdui.*;

public class Base64 extends Module {
    private String mode;
    private boolean urlSafe;
    private ChoiceGroup cgUrlSafe;
    private final char[] B64TABLE = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
        'w', 'x', 'y', 'z', '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', '+', '/'
    };

    public Base64(TTCanvas c, Display d)
    {
        super(c, d);
        this.mode = "decode";
        this.urlSafe = false;
    }

    // Handle input according to chosen mode and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled) {
            if (mode == "encode") {
                String bits = "";
                for (int i = 0; i < input.length(); i++) {
                    char c = input.charAt(i);
                    String bStr = Integer.toBinaryString((int)c);
                    while (bStr.length() % 8 != 0) {
                        bStr = '0' + bStr;
                    }
                    bits += bStr;
                }
                while (bits.length() % 6 != 0) {
                    bits += '0';
                }
                char[] b64 = new char[bits.length() / 6];
                int i = 0;
                for (int j = 0; j < bits.length(); j += 6) {
                    String sextet = bits.substring(j, j + 6);
                    int tableIdx = Integer.parseInt(sextet, 2);
                    b64[i++] = B64TABLE[tableIdx];
                }
                output = new String(b64);
                while (output.length() % 4 != 0) {
                    output += '=';
                }
                if (urlSafe) {
                    output = output.replace('+', '-');
                    output = output.replace('/', '_');
                }
            } else if (mode == "decode") {
                input = input.replace('-', '+');
                input = input.replace('_', '/');
                input = input.replace('=', ' ').trim();
                String bits = "";
                for (int i = 0; i < input.length(); i++) {
                    for (int j = 0; j < B64TABLE.length; j++) {
                        if (B64TABLE[j] == input.charAt(i)) {
                            String sextet = Integer.toBinaryString(j);
                            while (sextet.length() < 6) {
                                sextet = '0' + sextet;
                            }
                            bits += sextet;
                        }
                    }
                }
                while (bits.length() % 8 != 0) {
                    bits = bits.substring(0, bits.length() - 1);
                }
                output = "";
                for (int i = 0; i < bits.length(); i += 8) {
                    String octet = bits.substring(i, i + 8);
                    char c = (char)Integer.parseInt(octet, 2);
                    output += c;
                }
            }
        }
        return output;
    }

    // Cycle between modes
    protected void interact(int dir)
    {
        if (mode == "decode") {
            mode = "encode";
        } else if (mode == "encode") {
            mode = "decode";
        }
    }

    // Draw the contents of the Base64 Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        g.drawString("Base64 " + mode, PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("Base64 settings");
        String[] onoff = { "ON", "OFF" };
        cgUrlSafe = new ChoiceGroup("URL-safe", Choice.EXCLUSIVE, onoff, null);
        settings.append(cgUrlSafe);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            if (cgUrlSafe.getSelectedIndex() == 0) {
                urlSafe = true;
            } else {
                urlSafe = false;
            }
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
