/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.microedition.lcdui.*;

public class Substitution extends Module {
    private String[] charsets = { "A-Z", "Z-A", "A-Ö", "Ö-A",
                                  "UTF-8", "Freq. ENG", "Freq. SWE" };
    private TextField tfRotation;
    private ChoiceGroup cgCharsets;
    private int charsetIdx;
    private int rotation;

    public Substitution(TTCanvas c, Display d)
    {
        super(c, d);
        this.charsetIdx = 0;
        this.rotation = 0;
    }

    // Handle input and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled) {
            String alphabet = "abcdefghijklmnopqrstuvwxyz";
            String chars = "";
            String selected = charsets[charsetIdx];
            if (selected == "A-Z") {
                chars = alphabet;
            } else if (selected == "Z-A") {
                chars = "zyxwvutsrqponmlkjihgfedcba";
            } else if (selected == "A-Ö") {
                alphabet += "åäö";
                chars = alphabet;
            } else if (selected == "Ö-A") {
                alphabet += "åäö";
                chars = "öäåzyxwvutsrqponmlkjihgfedcba";
            } else if (selected == "UTF-8") {
                chars = "";
                for (int i = 0; i < 256; i++) {
                    chars += (char)i;
                }
                alphabet = new String(chars);
            } else if (selected == "Freq. ENG") {
                chars = "etaoinshrwdlykcumfgpbvjxqz";
            } else if (selected == "Freq. SWE") {
                chars = "eanrtsildomkgvhfupäbcåöyjxwzq";
            }

            char[] result = new char[input.length()];
            for (int i = 0; i < input.length(); i++) {
                char c = input.charAt(i);
                boolean upper = false;
                if (c != Character.toLowerCase(c)) {
                    upper = true;
                    c = Character.toLowerCase(c);
                }
                for (int j = 0; j < chars.length(); j++) {
                    if (alphabet.charAt(j) == c) {
                        int idx = j + rotation;
                        while (idx >= chars.length()) {
                            idx -= chars.length();
                        }
                        while (idx < 0) {
                            idx += chars.length();
                        }
                        c = chars.charAt(idx);
                        break;
                    }
                }
                if (upper) {
                    c = Character.toUpperCase(c);
                }
                result[i] = c;
            }
            output = new String(result);
        }
        return output;
    }

    // Increment or decrement rotation
    protected void interact(int dir)
    {
        if (dir == Canvas.RIGHT) {
            rotation++;
        } else if (dir == Canvas.LEFT) {
            rotation--;
        }
    }

    // Draw the contents of the Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        String chars = charsets[charsetIdx];
        String rot = Integer.toString(rotation);
        String str = "ROT" + rot + " - chars: " + chars;
        g.drawString(str, PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("Substitution settings");
        cgCharsets = new ChoiceGroup("Charsets:", Choice.EXCLUSIVE, charsets, null);
        cgCharsets.setSelectedIndex(charsetIdx, true);
        settings.append(cgCharsets);
        tfRotation = new TextField("Rotation:", Integer.toString(rotation),
                                   4, TextField.NUMERIC);
        settings.append(tfRotation);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            charsetIdx = cgCharsets.getSelectedIndex();
            try {
                rotation = Integer.parseInt(tfRotation.getString());
            } catch (Exception e) {
                rotation = 0;
            }
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
