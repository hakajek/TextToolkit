/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.microedition.lcdui.*;

public class Textflow extends Module {
    private String[] flows = { "unchanged", "reversed" };
    private int idx;
    private String textflow;
    private ChoiceGroup cgTextflows;

    public Textflow(TTCanvas c, Display d)
    {
        super(c, d);
        this.idx = 1;
        this.textflow = flows[idx];
    }

    // Handle input according to chosen textflow mode and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled) {
            if (textflow == "reversed") {
                char[] reversed = new char[input.length()];
                int cIdx = 0;
                for (int i = input.length() - 1; i >= 0; i--) {
                    reversed[cIdx] = input.charAt(i);
                    cIdx++;
                }
                output = new String(reversed);
            }
        }
        return output;
    }

    // Cycle between textflow modes
    protected void interact(int dir)
    {
        if (dir == Canvas.RIGHT) {
            idx++;
        } else if (dir == Canvas.LEFT) {
            idx--;
        }
        if (idx < 0) {
            idx = flows.length - 1;
        } else if (idx >= flows.length) {
            idx = 0;
        }
        textflow = flows[idx];
    }

    // Draw the contents of the Textflow Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        g.drawString("Flow: " + textflow, PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("Textflow settings");
        cgTextflows = new ChoiceGroup("Textflows:", Choice.EXCLUSIVE, flows, null);
        settings.append(cgTextflows);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            textflow = cgTextflows.getString(cgTextflows.getSelectedIndex());
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
