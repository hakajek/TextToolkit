/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Vector;
import javax.microedition.lcdui.*;

public class TTCanvas extends Canvas implements CommandListener {
    private TextToolkit midlet;
    private Display display;
    private Command exitCMD;
    private Command modListCMD;
    private Command addModCMD;
    private Command settingsCMD;
    private Command toggleCMD;
    private Command deleteCMD;
    private Command moveUpCMD;
    private Command moveDownCMD;
    private Command backCMD;
    private boolean menuActive;
    private List moduleList;
    private Input inputMod;
    private Output outputMod;
    private Vector stack;
    private int idx;
    private String[] moduleTypes = {
        "Base64", "Conversion", "Morse code", "Replace",
        "Substitution", "Textflow", "Vigenère", "XOR"
    };

    public TTCanvas(TextToolkit midlet)
    {
        this.midlet = midlet;
        this.display = Display.getDisplay(midlet);

        // Menu commands
        this.exitCMD = new Command("Exit", Command.EXIT, 1);
        this.modListCMD = new Command("New module", Command.ITEM, 1);
        this.addModCMD = new Command("Add", Command.ITEM, 1);
        this.settingsCMD = new Command("Settings", Command.ITEM, 1);
        this.toggleCMD = new Command("Toggle", Command.ITEM, 1);
        this.deleteCMD = new Command("Delete", Command.ITEM, 1);
        this.moveUpCMD = new Command("Move up", Command.ITEM, 1);
        this.moveDownCMD = new Command("Move down", Command.ITEM, 1);
        this.backCMD = new Command("Cancel", Command.BACK, 1);
        this.addCommand(exitCMD);
        this.addCommand(modListCMD);
        this.addCommand(settingsCMD);
        this.addCommand(toggleCMD);
        this.addCommand(deleteCMD);
        this.addCommand(moveUpCMD);
        this.addCommand(moveDownCMD);
        this.setCommandListener(this);
        this.menuActive = false;

        // Create List-Screen with available Module types
        this.moduleList = new List("Add module", Choice.IMPLICIT,
            moduleTypes, null);
        moduleList.addCommand(addModCMD);
        moduleList.addCommand(backCMD);
        moduleList.setCommandListener(this);

        // Create Input and Output elements
        this.stack = new Vector();
        this.idx = 0;
        inputMod = new Input(this, display);
        outputMod = new Output(this, display);
        stack.addElement(inputMod);
        stack.addElement(outputMod);
        Screen settings = inputMod.getSettings();
        display.setCurrent(settings);
    }

    // Return a monospace font
    public Font getMainFont()
    {
        return Font.getFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN,
            Font.SIZE_SMALL);
    }

    public Font getBoldFont()
    {
        return Font.getFont(Font.FACE_MONOSPACE, Font.STYLE_BOLD,
            Font.SIZE_MEDIUM);
    }

    // Draw the UI
    public void paint(Graphics g)
    {
        Module mod = (Module)stack.elementAt(0);
        int modHeight = mod.HEIGHT + mod.PADDING / 2;
        int nMods = (int)(getHeight() * .75 / modHeight);
        int ofs = idx - nMods / 2;
        if (ofs < 0) {
            ofs = 0;
        }
        g.setColor(255, 255, 255);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setFont(getMainFont());

        // Compile result
        String result = "";
        for (int i = 0; i < stack.size(); i++) {
            Module m = (Module)stack.elementAt(i);
            result = m.format(result);
        }

        // Draw modules
        boolean selected;
        for (int i = ofs; i <= ofs + nMods; i++) {
            Module m = (Module)stack.elementAt(i);
            if (i == idx) {
                selected = true;
            } else {
                selected = false;
            }
            m.paint(g, getWidth(), i - ofs, selected);
        }
    }

    // Handle keypresses
    public void keyPressed(int keyCode)
    {
        if (keyCode == -6) {
            menuActive = true;
        } else if (keyCode == -7) {
            menuActive = false;
        } else if (!menuActive) {
            int key = getGameAction(keyCode);
            Module m = selectedMod();
            switch (key) {
            case Canvas.UP:
                if (idx > 0) {
                    idx--;
                }
                break;
            case Canvas.DOWN:
                if (idx < stack.size() - 1) {
                    idx++;
                }
                break;
            case Canvas.LEFT:
            case Canvas.RIGHT:
                m.interact(key);
                break;
            }
            repaint();
        }
    }

    // Get the selected Module
    public Module selectedMod()
    {
        return (Module)stack.elementAt(idx);
    }

    // Create a module corresponding to an index in moduleTypes array
    public Module makeMod(int index)
    {
        Module m;
        switch (index) {
        case 0:
            m = new Base64(this, display);
            break;
        case 1:
            m = new Conversion(this, display);
            break;
        case 2:
            m = new Morse(this, display);
            break;
        case 3:
            m = new Replace(this, display);
            break;
        case 4:
            m = new Substitution(this, display);
            break;
        case 5:
            m = new Textflow(this, display);
            break;
        case 6:
            m = new Vigenere(this, display);
            break;
        case 7:
            m = new Xor(this, display);
            break;
        default:
            m = new Module(this, display);
        }
        return m;
    }

    // Handle menu commands
    public void commandAction(Command c, Displayable d)
    {
        if (c == exitCMD) {
            midlet.exit();
        } else if (c == modListCMD) {
            if (idx < stack.size() - 1) {
                menuActive = false;
                display.setCurrent(moduleList);
            } else {
                Alert a = new Alert("Can't add modules there.");
                display.setCurrent(a);
            }
        } else if (c == addModCMD) {
            List l = (List)d;
            Module m = makeMod(l.getSelectedIndex());
            stack.insertElementAt(m, ++idx);
            display.setCurrent(this);
        } else if (c == settingsCMD) {
            menuActive = false;
            Module m = selectedMod();
            Screen settings = m.getSettings();
            display.setCurrent(settings);
        } else if (c == toggleCMD) {
            menuActive = false;
            Module m = selectedMod();
            m.toggle();
            repaint();
        } else if (c == deleteCMD) {
            menuActive = false;
            if (idx > 0 && idx < stack.size() - 1) {
                stack.removeElementAt(idx--);
                repaint();
            } else {
                Alert a = new Alert("Can't delete module.");
                display.setCurrent(a);
            }
        } else if (c == moveUpCMD) {
            menuActive = false;
            if (idx > 1 && idx < stack.size() - 1) {
                Module m = selectedMod();
                stack.removeElement(m);
                stack.insertElementAt(m, --idx);
                repaint();
            } else {
                Alert a = new Alert("Can't move module up.");
                display.setCurrent(a);
            }
        } else if (c == moveDownCMD) {
            menuActive = false;
            if (idx > 0 && idx < stack.size() - 2) {
                Module m = selectedMod();
                stack.removeElement(m);
                stack.insertElementAt(m, ++idx);
                repaint();
            } else {
                Alert a = new Alert("Can't move module down.");
                display.setCurrent(a);
            }
        } else if (c == backCMD) {
            display.setCurrent(this);
        }
    }
}
