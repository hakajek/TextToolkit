/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.microedition.lcdui.*;

public class Input extends Module {
    private String text;
    private TextBox tfInput;

    public Input(TTCanvas c, Display d)
    {
        super(c, d);
        this.text = "Input";
    }

    // Return input
    protected String format(String input)
    {
        return this.text;
    }

    // Draw the contents of the Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        Font f = canvas.getMainFont();
        String str = text;
        int strWidth = f.stringWidth(str);
        if (strWidth > width - PADDING) {
            str = "";
            for (int i = 0; i < text.length(); i++) {
                if (f.stringWidth(str + "...") < width - PADDING) {
                    str += text.charAt(i);
                } else {
                    str += "...";
                    break;
                }
            }
        }
        g.drawString(str, PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Return a TextBox for input
    protected Screen getSettings()
    {
        tfInput = new TextBox("Input", text, 500, TextField.ANY);
        tfInput.addCommand(saveCMD);
        tfInput.addCommand(cancelCMD);
        tfInput.setCommandListener(this);
        return (Screen)tfInput;
    }

    // Handle command events
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            text = tfInput.getString();
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
