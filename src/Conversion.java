/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Vector;
import javax.microedition.lcdui.*;

public class Conversion extends Module {
    private String[] inEncodings = { "AUTO", "TEXT", "HEX", "DEC", "BIN" };
    private String[] outEncodings = { "TEXT", "HEX", "DEC", "BIN" };
    private String inEncoding;
    private String outEncoding;
    private ChoiceGroup cgIn;
    private ChoiceGroup cgOut;
    private int idx;

    public Conversion(TTCanvas c, Display d)
    {
        super(c, d);
        this.idx = 0;
        this.inEncoding = inEncodings[idx];
        this.outEncoding = outEncodings[idx];
    }

    // Handle input according to chosen encodings and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled) {
            try {
                output = "";
                String enc = inEncoding;
                if (enc == "AUTO") {
                    enc = identEncoding(input);
                }
                if (enc == "TEXT") {
                    output = textToX(input);
                } else {
                    Vector words = split(input, ' ');
                    for (int i = 0; i < words.size(); i++) {
                        if (enc == "HEX") {
                            output += hexToX((String)words.elementAt(i));
                        } else if (enc == "DEC") {
                            output += decToX((String)words.elementAt(i));
                        } else if (enc == "BIN") {
                            output += binToX((String)words.elementAt(i));
                        }
                        if (outEncoding != "TEXT") {
                            output += ' ';
                        }
                    }
                }
            } catch (Exception e) {
                // System.out.println(e);
            }
        }
        return output;
    }

    // Convert from inEncoding to outEncoding
    protected String textToX(String inStr)
    {
        String outStr = "";
        for (int i = 0; i < inStr.length(); i++) {
            int val = (int)inStr.charAt(i);
            if (outEncoding == "HEX") {
                String hex = Integer.toHexString(val);
                outStr += moduloPad(hex, 2) + ' ';
            } else if (outEncoding == "DEC") {
                outStr += Integer.toString(val) + ' ';
            } else if (outEncoding == "BIN") {
                String octal = Integer.toBinaryString(val);
                outStr += moduloPad(octal, 8) + ' ';
            } else {
                outStr += inStr.charAt(i);
            }
        }
        return outStr;
    }

    protected String hexToX(String inStr)
    {
        String outStr = "";
        if (outEncoding == "TEXT") {
            Vector pairs = split(inStr, 2);
            for (int i = 0; i < pairs.size(); i++) {
                String pair = (String)pairs.elementAt(i);
                int val = Integer.parseInt(pair, 16);
                outStr += (char)val;
            }
        } else if (outEncoding == "DEC") {
            int val = Integer.parseInt(inStr, 16);
            outStr = Integer.toString(val);
        } else if (outEncoding == "BIN") {
            int val = Integer.parseInt(inStr, 16);
            String octal = Integer.toBinaryString(val);
            outStr = moduloPad(octal, 8);
        } else {
            outStr = moduloPad(inStr, 2);
        }
        return outStr;
    }

    protected String decToX(String inStr)
    {
        String outStr = "";
        int val = Integer.parseInt(inStr);
        if (outEncoding == "TEXT") {
            outStr += (char)val;
        } else if (outEncoding == "HEX") {
            String pair = Integer.toHexString(val);
            outStr = moduloPad(pair, 2);
        } else if (outEncoding == "BIN") {
            String octal = Integer.toBinaryString(val);
            outStr = moduloPad(octal, 8);
        } else {
            outStr = inStr;
        }
        return outStr;
    }

    protected String binToX(String inStr)
    {
        String outStr = "";
        int val = Integer.parseInt(inStr, 2);
        if (outEncoding == "TEXT") {
            Vector octets = split(inStr, 8);
            for (int i = 0; i < octets.size(); i++) {
                String octet = (String)octets.elementAt(i);
                val = Integer.parseInt(octet, 2);
                outStr += (char)val;
            }
        } else if (outEncoding == "HEX") {
            String pair = Integer.toHexString(val);
            outStr = moduloPad(pair, 2);
        } else if (outEncoding == "DEC") {
            outStr = Integer.toString(val);
        } else {
            outStr = moduloPad(inStr, 8);
        }
        return outStr;
    }

    // Determine the proper encoding of the contents of a String
    protected String identEncoding(String str)
    {
        char[] hexChars = {
            '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F', ' '
        };
        boolean bin = true;
        boolean dec = true;
        boolean hex = true;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c != '0' && c != '1' && c != ' ') {
                bin = false;
            }
            if (!Character.isDigit(c) && c != ' ') {
                dec = false;
            }
            boolean exists = false;
            for (int j = 0; j < hexChars.length; j++) {
                if (Character.toUpperCase(c) == hexChars[j]) {
                    exists = true;
                }
            }
            if (!exists) {
                hex = false;
                break;
            }
        }
        String encoding;
        if (bin) {
            encoding = "BIN";
        } else if (dec) {
            encoding = "DEC";
        } else if (hex) {
            encoding = "HEX";
        } else {
            encoding = "TEXT";
        }
        return encoding;
    }

    // Cycle between encodings
    protected void interact(int dir)
    {
        if (dir == Canvas.RIGHT) {
            idx++;
        } else if (dir == Canvas.LEFT) {
            idx--;
        }
        if (idx < 0) {
            idx = outEncodings.length - 1;
        } else if (idx >= outEncodings.length) {
            idx = 0;
        }
        outEncoding = outEncodings[idx];
    }

    // Draw the contents of the Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        g.drawString("Convert " + inEncoding + " -> " + outEncoding,
            PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("Conversion settings");
        cgIn = new ChoiceGroup("From:", Choice.EXCLUSIVE, inEncodings, null);
        cgOut = new ChoiceGroup("To:", Choice.EXCLUSIVE, outEncodings, null);
        settings.append(cgIn);
        settings.append(cgOut);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            inEncoding = cgIn.getString(cgIn.getSelectedIndex());
            outEncoding = cgOut.getString(cgOut.getSelectedIndex());
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
