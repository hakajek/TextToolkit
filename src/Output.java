/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Vector;
import javax.microedition.lcdui.*;

public class Output extends Module {
    private Vector output;
    private int idx;
    private int nRows;

    public Output(TTCanvas c, Display d)
    {
        super(c, d);
        this.output = new Vector();
        this.idx = 0;
        this.nRows = (canvas.getHeight() - 34) / CHARHEIGHT - 1;
    }

    // Handle input and return result
    protected String format(String input)
    {
        // split input into individual words
        output.removeAllElements();
        Font f = canvas.getMainFont();
        Vector words = new Vector();
        String word = "";
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == ' ') {
                words.addElement(word);
                word = "";
            } else if (f.stringWidth(word + c + "   ") >= canvas.getWidth()) {
                words.addElement(word + '-');
                word = "" + c;
            } else {
                word += c;
            }
        }
        words.addElement(word);

        // concatenate words
        String str = "";
        for (int i = 0; i < words.size(); i++) {
            word = (String)words.elementAt(i);
            if (f.stringWidth(str + word + ' ') < canvas.getWidth()) {
                str += word + ' ';
            } else {
                output.addElement(str);
                str = word + ' ';
            }
        }
        output.addElement(str);
        return input;
    }

    // Cycle rows of text
    protected void interact(int dir)
    {
        if (dir == Canvas.RIGHT && idx < output.size() - 1) {
            idx++;
        } else if (dir == Canvas.LEFT && idx > 0) {
            idx--;
        }
    }

    // Draw the Module
    protected void paint(Graphics g, int formWidth, int index, boolean selected)
    {
        int yPos = HEIGHT * index + (PADDING / 2) * index + PADDING / 2;
        if (selected) {
            g.setColor(COLOR_SELECTED);
        } else {
            g.setColor(TEXTCOLOR);
        }
        g.setFont(canvas.getBoldFont());
        if (idx >= output.size()) {
            idx = output.size() - 1;
        }
        nRows = (canvas.getHeight() - yPos) / CHARHEIGHT - 1;
        int lastVisible = idx + nRows;
        if (lastVisible > output.size()) {
            lastVisible = output.size();
        }
        String res = Integer.toString(idx + 1) + "-"
                     + Integer.toString(lastVisible) + "/"
                     + Integer.toString(output.size());
        g.drawString(res, formWidth - PADDING, yPos + PADDING, g.TOP | g.RIGHT);
        g.drawString("Result", PADDING, yPos + PADDING, g.TOP | g.LEFT);
        g.setColor(TEXTCOLOR);
        g.setFont(canvas.getMainFont());
        for (int i = 0; i < nRows; i++) {
            if (i + idx >= 0 && i + idx < output.size()) {
                String str = (String)output.elementAt(i + idx);
                g.drawString(str, PADDING,
                             yPos + PADDING + CHARHEIGHT * (i + 1),
                             g.TOP | g.LEFT);
            }
        }
    }
}
