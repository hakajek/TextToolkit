/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.*;

public class Morse extends Module {
    private String mode;
    private String dot;
    private String dash;
    private String delimiter;
    private TextField tfDot;
    private TextField tfDash;
    private TextField tfDelimiter;
    private Hashtable table;

    public Morse(TTCanvas c, Display d)
    {
        super(c, d);
        this.mode = "decode";
        this.dot = ".";
        this.dash = "-";
        this.delimiter = " ";
        this.table = new Hashtable();
        table.put("A", ".-");
        table.put("B", "-...");
        table.put("C", "-.-.");
        table.put("D", "-..");
        table.put("E", ".");
        table.put("F", "..-.");
        table.put("G", "--.");
        table.put("H", "....");
        table.put("I", "..");
        table.put("J", ".---");
        table.put("K", "-.-");
        table.put("L", ".-..");
        table.put("M", "--");
        table.put("N", "-.");
        table.put("O", "---");
        table.put("P", ".--.");
        table.put("Q", "--.-");
        table.put("R", ".-.");
        table.put("S", "...");
        table.put("T", "-");
        table.put("U", "..-");
        table.put("V", "...-");
        table.put("W", ".--");
        table.put("X", "-..-");
        table.put("Y", "-.--");
        table.put("Z", "--..");
        table.put("1", ".----");
        table.put("2", "..---");
        table.put("3", "...--");
        table.put("4", "....-");
        table.put("5", ".....");
        table.put("6", "-....");
        table.put("7", "--...");
        table.put("8", "---..");
        table.put("9", "----.");
        table.put("0", "-----");
    }

    // Handle input according to chosen mode and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled) {
            if (mode == "encode") {
                output = "";
                for (int i = 0; i < input.length(); i++) {
                    String c = "" + Character.toUpperCase(input.charAt(i));
                    if (table.containsKey(c)) {
                        String code = (String)table.get(c);
                        code = code.replace('.', dot.charAt(0));
                        code = code.replace('-', dash.charAt(0));
                        output += code + delimiter;
                    }
                }
            } else if (mode == "decode") {
                output = "";
                Vector words = split(input, delimiter.charAt(0));
                for (int i = 0; i < words.size(); i++) {
                    String code = (String)words.elementAt(i);
                    code = code.replace(dot.charAt(0), '.');
                    code = code.replace(dash.charAt(0), '-');
                    if (table.contains(code)) {
                        for (Enumeration k = table.keys(); k.hasMoreElements();) {
                            String key = (String)k.nextElement();
                            if (code.equals(table.get(key))) {
                                output += key;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return output;
    }

    // Cycle between modes
    protected void interact(int dir)
    {
        if (mode == "decode") {
            mode = "encode";
        } else if (mode == "encode") {
            mode = "decode";
        }
    }

    // Draw the contents of the Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        g.drawString("Morse code " + mode, PADDING,
            yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("Morse code settings");
        tfDot = new TextField("Short:", dot, 1, TextField.ANY);
        tfDash = new TextField("Long:", dash, 1, TextField.ANY);
        tfDelimiter = new TextField("Delimiter:", delimiter, 1, TextField.ANY);
        settings.append(tfDot);
        settings.append(tfDash);
        settings.append(tfDelimiter);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            if (tfDot.size() == 1) {
                dot = tfDot.getString();
            } else {
                dot = ".";
            }
            if (tfDash.size() == 1) {
                dash = tfDash.getString();
            } else {
                dash = "-";
            }
            if (tfDelimiter.size() == 1) {
                delimiter = tfDelimiter.getString();
            } else {
                delimiter = " ";
            }
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
