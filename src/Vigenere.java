/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.microedition.lcdui.*;

public class Vigenere extends Module {
    private String[] charsets = { "A-Z", "A-Ö", "Freq. ENG", "Freq. SWE" };
    private TextField tfKey;
    private String key;
    private ChoiceGroup cgCharsets;
    private int charsetIdx;
    private ChoiceGroup cgMode;
    private String mode;

    public Vigenere(TTCanvas c, Display d)
    {
        super(c, d);
        this.charsetIdx = 0;
        this.key = "key";
        this.mode = "decode";
    }

    // Handle input and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled) {
            String selected = charsets[charsetIdx];
            String chars = "";
            if (selected == "A-Z") {
                chars = "abcdefghijklmnopqrstuvwxyz";
            } else if (selected == "Z-A") {
                chars = "zyxwvutsrqponmlkjihgfedcba";
            } else if (selected == "A-Ö") {
                chars = "abcdefghijklmnopqrstuvwxyzåäö";
            } else if (selected == "Ö-A") {
                chars = "öäåzyxwvutsrqponmlkjihgfedcba";
            } else if (selected == "Freq. ENG") {
                chars = "etaoinshrwdlykcumfgpbvjxqz";
            } else if (selected == "Freq. SWE") {
                chars = "eanrtsildomkgvhfupäbcåöyjxwzq";
            }

            key = key.toLowerCase();
            char[] result = new char[input.length()];
            int inpIdx = 0;
            for (int i = 0; i < input.length(); i++) {
                char c = input.charAt(i);
                boolean upper = false;
                if (Character.toLowerCase(c) != c) {
                    upper = true;
                    c = Character.toLowerCase(c);
                }
                boolean exists = false;
                for (int j = 0; j < chars.length(); j++) {
                    if (c == chars.charAt(j)) {
                        exists = true;
                        break;
                    }
                }
                if (exists) {
                    int keyIdx = inpIdx;
                    while (keyIdx >= key.length()) {
                        keyIdx -= key.length();
                    }
                    int keyCharIdx = 0;
                    int inpCharIdx = 0;
                    for (int j = 0; j < chars.length(); j++) {
                        if (key.charAt(keyIdx) == chars.charAt(j)) {
                            keyCharIdx = j;
                        }
                        if (c == chars.charAt(j)) {
                            inpCharIdx = j;
                        }
                    }
                    if (mode == "encode") {
                        int resCharIdx = inpCharIdx + keyCharIdx;
                        while (resCharIdx >= chars.length()) {
                            resCharIdx -= chars.length();
                        }
                        c = chars.charAt(resCharIdx);
                    } else if (mode == "decode") {
                        int resCharIdx = inpCharIdx - keyCharIdx;
                        while (resCharIdx < 0) {
                            resCharIdx += chars.length();
                        }
                        c = chars.charAt(resCharIdx);
                    }
                    inpIdx++;
                }
                if (upper) {
                    c = Character.toUpperCase(c);
                }
                result[i] = c;
            }
            output = new String(result);
        }
        return output;
    }

    // Toggle mode
    protected void interact(int dir)
    {
        if (mode == "encode") {
            mode = "decode";
        } else {
            mode = "encode";
        }
    }

    // Draw the contents of the Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        String str = "Vigenère " + mode;
        g.drawString(str, PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("Vigenère settings");
        cgCharsets = new ChoiceGroup("Charsets:", Choice.EXCLUSIVE, charsets, null);
        cgCharsets.setSelectedIndex(charsetIdx, true);
        settings.append(cgCharsets);
        tfKey = new TextField("Key:", key, 50, TextField.ANY);
        settings.append(tfKey);
        String[] modes = { "encode", "decode" };
        cgMode = new ChoiceGroup("Mode:", Choice.EXCLUSIVE, modes, null);
        if (mode == "encode") {
            cgMode.setSelectedIndex(0, true);
        } else {
            cgMode.setSelectedIndex(1, true);
        }
        settings.append(cgMode);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            charsetIdx = cgCharsets.getSelectedIndex();
            key = tfKey.getString();
            mode = cgMode.getString(cgMode.getSelectedIndex());
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
