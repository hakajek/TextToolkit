/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Vector;
import javax.microedition.lcdui.*;

public class Module implements CommandListener {
    protected TTCanvas canvas;
    protected Display display;
    protected boolean disabled;
    protected Command cancelCMD;
    protected Command saveCMD;
    protected final int COLOR = 0x00DDDDDD;
    protected final int COLOR_DISABLED = 0x00D2D2D2;
    protected final int COLOR_SELECTED = 0x0099AAEE;
    protected final int TEXTCOLOR = 0x00000000;
    protected final int TEXTCOLOR_DISABLED = 0x00999999;
    protected final int HEIGHT;
    protected final int CHARHEIGHT;
    protected final int PADDING;

    public Module(TTCanvas c, Display d)
    {
        this.canvas = c;
        this.display = d;
        this.disabled = false;
        this.cancelCMD = new Command("Cancel", Command.BACK, 1);
        this.saveCMD = new Command("Save", Command.OK, 1);
        this.PADDING = 6;
        this.CHARHEIGHT = c.getMainFont().getHeight();
        this.HEIGHT = (int)(this.CHARHEIGHT * 1.6);
    }

    // Draw the Module
    protected void paint(Graphics g, int formWidth, int index, boolean selected)
    {
        int width = formWidth - PADDING;
        int yPos = HEIGHT * index + (PADDING / 2) * index + PADDING / 2;
        if (selected) {
            g.setColor(COLOR_SELECTED);
        } else if (disabled) {
            g.setColor(COLOR_DISABLED);
        } else {
            g.setColor(COLOR);
        }
        g.fillRect(PADDING / 2, yPos, width, HEIGHT);
        if (disabled) {
            g.setColor(TEXTCOLOR_DISABLED);
        } else {
            g.setColor(TEXTCOLOR);
        }
        paintContent(g, yPos, width);
    }

    // Draw the Module's contents - overridden by each sub-Module
    protected void paintContent(Graphics g, int yPos, int width) { }

    // Determine Module's behavior on interaction - overridden by sub-Modules
    protected void interact(int dir) { }

    // Determine how Module manipulates data - overridden by sub-Modules
    protected String format(String input)
    {
        String output = input;
        return output;
    }

    // Disable or enable the Module
    protected void toggle()
    {
        if (disabled) {
            disabled = false;
        } else {
            disabled = true;
        }
    }

    // Split a String by a given single character delimiter
    protected Vector split(String str, char del)
    {
        Vector words = new Vector();
        String word = "";
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == del) {
                words.addElement(word);
                word = "";
            } else {
                word += c;
            }
        }
        words.addElement(word);
        return words;
    }

    // Split a String into chunks of a given length
    protected Vector split(String str, int len)
    {
        Vector chunks = new Vector();
        for (int i = 0; i < str.length(); i += len) {
            if (i + len <= str.length()) {
                String chunk = str.substring(i, i + len);
                chunks.addElement(chunk);
            } else {
                String chunk = str.substring(i, str.length());
                chunks.addElement(moduloPad(chunk, len));
            }
        }
        return chunks;
    }

    // Pad a String with leading 0s so its length % a number == 0
    protected String moduloPad(String str, int modulo)
    {
        while (str.length() % modulo != 0) {
            str = '0' + str;
        }
        return str;
    }

    // Get a Form with the Module's settings - overridden by sub-Modules
    protected Screen getSettings()
    {
        Alert a = new Alert("There are no settings for this module.");
        return (Screen)a;
    }

    // Handle command events
    public void commandAction(Command c, Displayable d) { }
}
