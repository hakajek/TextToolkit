/*
 * Copyright 2023 hakajek
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.microedition.lcdui.*;

public class Replace extends Module {
    private String mode;
    private String set1;
    private String set2;
    private TextField tfSet1;
    private TextField tfSet2;

    public Replace(TTCanvas c, Display d)
    {
        super(c, d);
        this.mode = "on";
        this.set1 = "";
        this.set2 = "";
    }

    // Handle input according to chosen mode and return result
    protected String format(String input)
    {
        String output = input;
        if (!disabled) {
            if (mode == "on") {
                int minLength = Math.min(set1.length(), set2.length());
                for (int i = 0; i < minLength; i++) {
                    char c1 = set1.charAt(i);
                    char c2 = set2.charAt(i);
                    output = output.replace(c1, c2);
                }
            }
        }
        return output;
    }

    // Cycle between modes
    protected void interact(int dir)
    {
        if (mode == "off") {
            mode = "on";
        } else if (mode == "on") {
            mode = "off";
        }
    }

    // Draw the contents of the Module
    protected void paintContent(Graphics g, int yPos, int width)
    {
        g.drawString("Replace [" + mode + "]: [" + set1 + "], [" + set2 + "]",
                     PADDING, yPos + PADDING, g.TOP | g.LEFT);
    }

    // Generate a Form with settings
    protected Screen getSettings()
    {
        Form settings = new Form("Replace settings");
        tfSet1 = new TextField("Set 1:", set1, 50, TextField.ANY);
        tfSet2 = new TextField("Set 2:", set2, 50, TextField.ANY);
        settings.append(tfSet1);
        settings.append(tfSet2);
        settings.addCommand(saveCMD);
        settings.addCommand(cancelCMD);
        settings.setCommandListener(this);
        return (Screen)settings;
    }

    // Handle events in the settings Form
    public void commandAction(Command c, Displayable d)
    {
        if (c == saveCMD) {
            set1 = tfSet1.getString();
            set2 = tfSet2.getString();
            display.setCurrent(canvas);
        } else if (c == cancelCMD) {
            display.setCurrent(canvas);
        }
    }
}
